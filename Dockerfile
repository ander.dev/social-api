FROM gradle:latest AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src

RUN gradle build --no-daemon -x test --info

FROM openjdk:8-jre-alpine

RUN apk add --no-cache tzdata
ENV TZ Pacific/Auckland

ENV SOCIAL_API_MYSQL_DATABASE=${SOCIAL_API_MYSQL_DATABASE}
ENV SOCIAL_API_MYSQL_USER=${SOCIAL_API_MYSQL_USER}
ENV SOCIAL_API_MYSQL_PASSWORD=${SOCIAL_API_MYSQL_PASSWORD}
ENV SOCIAL_API_MYSQL_HOST=${SOCIAL_API_MYSQL_HOST}
ENV SOCIAL_API_MYSQL_PORT=${SOCIAL_API_MYSQL_PORT}
ENV SOCIAL_API_HOST_PROTOCOL=${SOCIAL_API_HOST_PROTOCOL}
ENV SOCIAL_API_HOST_URL=${SOCIAL_API_HOST_URL}
ENV SOCIAL_API_HOST_PORT=${SOCIAL_API_HOST_PORT}
ENV SOCIAL_API_BASIC_USER=${SOCIAL_API_BASIC_USER}
ENV SOCIAL_API_BASIC_PASSWORD=${SOCIAL_API_BASIC_PASSWORD}
ENV SOCIAL_API_LOG_PATH=${SOCIAL_API_LOG_PATH}
ENV SOCIAL_API_LOG_LEVEL=${SOCIAL_API_LOG_LEVEL}

EXPOSE ${SOCIAL_API_HOST_PORT}

RUN mkdir /app
RUN mkdir /app/logs

WORKDIR /app

COPY --from=build /home/gradle/src/build/libs/*.jar /app/spring-boot-application.jar

ENTRYPOINT ["java", "-XX:+UnlockExperimentalVMOptions", "-XX:+UseCGroupMemoryLimitForHeap", "-Djava.security.egd=file:/dev/./urandom","-jar","/app/spring-boot-application.jar"]
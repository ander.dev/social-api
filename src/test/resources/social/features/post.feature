@post
Feature: Posts tests

  Background: Load userDTO from minimum
    Given a new "user" is loaded using "user-minimum"
    And user loaded is submitted

  Scenario: New post without User
    Given a new "post" is loaded using "post-minimum"
    When post loaded is submitted
    Then response should contain message "User must be informed!"
    And response http status should be 400
    And traceId should be present

  Scenario: New post without message
    Given a new "post" is loaded using "post-minimum"
    And post loaded does not have message
    And post loaded contains user just created
    When post loaded is submitted
    Then response should contain message "Post message must be informed!"
    And response http status should be 400
    And traceId should be present

  Scenario: New post successfully created
    Given a new "post" is loaded using "post-minimum"
    And post loaded contains user just created
    When post loaded is submitted
    Then response should contain message "Post successfully saved!"
    And response http status should be 200
    And traceId should be present

  Scenario: New post successfully listed
    Given a new "post" is loaded using "post-minimum"
    And post loaded contains user just created
    And post loaded is submitted
    And response should contain message "Post successfully saved!"
    Given a new "post" is loaded using "post-minimum"
    And post loaded contains user just created
    And post loaded is submitted
    And response should contain message "Post successfully saved!"
    When the list of post for current user is request
    Then response should contain message "Posts successfully listed!"
    And response http status should be 200
    And response should contain 2 objects
    And traceId should be present

  Scenario: New post successfully edited
    Given a new "post" is loaded using "post-minimum"
    And post loaded contains user just created
    When post loaded is submitted
    Then response should contain message "Post successfully saved!"
    Given I get post just created
    And I change post message to "New post from automation"
    When post loaded is submitted
    Then response should contain message "Post successfully updated!"
    And post message is changed to "New post from automation"


  Scenario: Delete post
    Given a new "post" is loaded using "post-minimum"
    And post loaded contains user just created
    When post loaded is submitted
    Then response should contain message "Post successfully saved!"
    Given a new "comment" is loaded using "comment-minimum"
    And comment loaded contains user just created
    And comment loaded contains post just created
    When comment loaded is submitted
    When I request to delete a Post just created
    Then response should contain message "Post and its Comments successfully deleted!"
    And response http status should be 200
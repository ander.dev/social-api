package social.steps

import io.cucumber.java8.En
import io.cucumber.java8.Scenario
import org.junit.Assert
import social.entity.dto.CommentDTO
import social.entity.dto.PostDTO
import social.entity.dto.UserDTO
import social.services.RestService

var commonStepsLastInstance: CommonSteps? = null


class CommonSteps : En, RestService() {

    init {

        Before { _: Scenario ->
            Assert.assertNotSame(this, commonStepsLastInstance)
            commonStepsLastInstance = this
        }

        After { _: Scenario ->
            Assert.assertSame(this, commonStepsLastInstance)
            commonStepsLastInstance = this
        }

        Given("a new {string} is loaded using {string}") { obj: String, minimum: String ->

            when (obj) {
                "user" -> {
                    userDTO = getDTO(minimum) as UserDTO
                    println(userDTO)
                }
                "post" -> {
                    postDTO = getDTO(minimum) as PostDTO
                    println(postDTO)
                }
                "comment" -> {
                    commentDTO = getDTO(minimum) as CommentDTO
                    println(postDTO)
                }
                else -> Assert.fail("Object must be defined.")
            }
        }

        Then("response should contain message {string}") { message: String ->
            Assert.assertTrue(responseDTO.messages?.contains(message)!!)
        }

        And("traceId should be present") {
            Assert.assertNotNull(responseDTO.traceId)
        }

        And("response http status should be {int}") { statusCode: Int ->
            Assert.assertEquals(responseDTO.statusCode, statusCode)
        }

        And("response should contain {int} objects") { number: Int ->
            Assert.assertEquals(responseDTO.objects!!.size, number)
        }
    }
}
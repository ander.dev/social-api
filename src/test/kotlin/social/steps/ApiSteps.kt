package social.steps

import io.cucumber.java8.En
import io.cucumber.java8.Scenario
import org.junit.Assert
import social.services.ApiStepsService

var apiStepsLastInstance: ApiSteps? = null


class ApiSteps : En, ApiStepsService() {

    init {

        Before { _: Scenario ->
            Assert.assertNotSame(this, apiStepsLastInstance)
            apiStepsLastInstance = this
        }

        After { _: Scenario ->
            Assert.assertSame(this, apiStepsLastInstance)
            apiStepsLastInstance = this
        }

        Given("I want to get status of the API") {
            responseDTO = get()
        }
    }
}
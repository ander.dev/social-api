package social.services

import social.entity.dto.ResponseDTO

open class ApiStepsService: RestService() {

    fun get(): ResponseDTO {
        return get("$BASE_URL")
    }

    companion object {
        const val BASE_URL = "/api"
    }
}
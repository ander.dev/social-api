package social.services

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import mu.KotlinLogging
import org.junit.Assert
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.*
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import social.entity.dto.CommentDTO
import social.entity.dto.PostDTO
import social.entity.dto.ResponseDTO
import social.entity.dto.UserDTO
import java.io.File
import java.nio.file.FileSystems


open class RestService {

    private val logger = KotlinLogging.logger {}

    @Autowired
    lateinit var apiURL: String

    @Autowired
    lateinit var securityPassword: String

    @Autowired
    lateinit var securityUser: String

    @Autowired
    lateinit var mapper: ObjectMapper

    @Autowired
    lateinit var testRestTemplate: TestRestTemplate

    private val headers = HttpHeaders()

    companion object {
        lateinit var responseDTO: ResponseDTO
        lateinit var userDTO: UserDTO
        lateinit var postDTO: PostDTO
        lateinit var commentDTO: CommentDTO
    }

    private fun mapJsonToObject(fileName: String): Any {
        val path = FileSystems.getDefault().getPath("src/test/kotlin/social/data", "$fileName.json")

        fileName.split("-").forEach {

            return when (it) {
                "user" -> mapper.readValue(File(path.toString())) as UserDTO
                "post" -> mapper.readValue(File(path.toString())) as PostDTO
                "comment" -> mapper.readValue(File(path.toString())) as CommentDTO
                else -> return Assert.fail("no DTO to be mapped to!")
            }
        }
        return Assert.fail("no DTO to be mapped to!")
    }

    fun getDTO(fileName: String): Any {
        return mapJsonToObject(fileName)
    }

    fun get(url: String): ResponseDTO {
        val resp = testRestTemplate.withBasicAuth(securityUser, securityPassword).getForEntity("$apiURL$url", String::class.java)
        responseDTO = mapper.readValue(resp.body, ResponseDTO::class.java)
        return responseDTO
    }

    fun post(url: String, obj: Any): ResponseDTO {
        headers.contentType = MediaType.APPLICATION_JSON
        val entity = HttpEntity(obj, headers)
        val resp = testRestTemplate.withBasicAuth(securityUser, securityPassword).postForEntity("$apiURL$url", entity, String::class.java)
        responseDTO = mapper.readValue(resp.body, ResponseDTO::class.java)
        return responseDTO
    }

    fun delete(url: String): ResponseDTO {
        val resp = testRestTemplate.withBasicAuth(securityUser, securityPassword).exchange("$apiURL$url", HttpMethod.DELETE, HttpEntity<Any>(headers), String::class.java)
        responseDTO = mapper.readValue(resp.body, ResponseDTO::class.java)
        return responseDTO
    }

    fun list(url: String): ResponseDTO {
        val resp = testRestTemplate.withBasicAuth(securityUser, securityPassword).getForEntity("$apiURL$url", String::class.java)
        responseDTO = mapper.readValue(resp.body, ResponseDTO::class.java)
        return responseDTO
    }

    fun login(email:String, password:String, url: String): ResponseDTO {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_FORM_URLENCODED

        val map: MultiValueMap<String, String> = LinkedMultiValueMap()
        map.add("email", email)
        map.add("password", password)

        val entity = HttpEntity(map, headers)

        val resp: ResponseEntity<String> = testRestTemplate.withBasicAuth(securityUser, securityPassword).postForEntity("$apiURL$url", entity, String::class.java)
        responseDTO = mapper.readValue(resp.body, ResponseDTO::class.java)
        return responseDTO
    }

}
package social.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import social.entity.dto.CommentDTO
import social.entity.dto.ResponseDTO
import social.service.CommentService
import java.util.*
import javax.ws.rs.core.MediaType

@RestController
@RequestMapping(path = ["/api/comment"], produces = [MediaType.APPLICATION_JSON])
class CommentController {

    @Autowired
    lateinit var commentService: CommentService

    @PostMapping()
    fun save (@RequestBody commentDTO: CommentDTO): ResponseDTO = commentService.save(commentDTO)

    @GetMapping("/by-user/{userId}")
    fun listByUser(@PathVariable("userId") userId: UUID):  ResponseDTO = commentService.findAllByUser(userId)

    @GetMapping("/by-post/{postId}/{userId}")
    fun listByPostAndUser(@PathVariable("postId") postId: UUID, @PathVariable("userId") userId: UUID):  ResponseDTO = commentService.findAllByPostAndUser(postId, userId)

    @GetMapping("/{id}")
    fun get(@PathVariable("id") id: UUID): ResponseDTO = commentService.findById(id)

    @RequestMapping("/{commentId}/{userId}", method = [(RequestMethod.DELETE)])
    fun delete (@PathVariable("commentId") commentId: UUID, @PathVariable("userId") userId: UUID): ResponseDTO = commentService.delete(commentId, userId)

}
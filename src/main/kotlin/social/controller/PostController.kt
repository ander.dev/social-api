package social.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import social.entity.dto.PostDTO
import social.entity.dto.ResponseDTO
import social.service.PostService
import java.util.*
import javax.ws.rs.core.MediaType

@RestController
@RequestMapping(path = ["/api/post"], produces = [MediaType.APPLICATION_JSON])
class PostController {

    @Autowired
    lateinit var postService: PostService

    @PostMapping()
    fun save (@RequestBody postDTO: PostDTO): ResponseDTO = postService.save(postDTO)

    @GetMapping("/by-user/{userId}")
    fun list(@PathVariable("userId") userId: UUID):  ResponseDTO = postService.findAllByUser(userId)

    @GetMapping
    fun list():  ResponseDTO = postService.findAll()

    @GetMapping("/{id}")
    fun get(@PathVariable("id") id: UUID): ResponseDTO = postService.findById(id)

    @RequestMapping("/{postId}/{userId}", method = [(RequestMethod.DELETE)])
    fun delete (@PathVariable("postId") postId: UUID, @PathVariable("userId") userId: UUID): ResponseDTO = postService.delete(postId, userId)

}
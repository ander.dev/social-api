package social.entity.dto

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.LocalDateTime
import java.util.*

data class PostDTO(
        var id: UUID? = null,
        var message: String? = null,
        var user: UserDTO? = null,
        @get:JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        var created: LocalDateTime? = null,
        var commentList: MutableList<CommentDTO>? = ArrayList()
) {
    override fun toString(): String {
        return "PostDTO(id=$id, message='$message', created=$created, commentList=$commentList, user=$user)"
    }
}

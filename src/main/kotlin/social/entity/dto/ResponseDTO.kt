package social.entity.dto

import com.fasterxml.jackson.annotation.JsonRootName
import org.slf4j.MDC
import java.util.*

@JsonRootName("response")
data class ResponseDTO(
        var success: Boolean,
        var statusCode: Int,
        var traceId: String? = null,
        var timestamp: Date,
        var objects: MutableList<Any>? = null,
        var messages: MutableList<String>? = null
) {
    override fun toString(): String {
        return "ResponseDTO(statusCode=$statusCode, success=$success, traceId=$traceId, timestamp=$timestamp, objects=$objects, messages=$messages)"
    }

    companion object {
        fun success(statusCode: Int, objects: MutableList<Any>?, messages: MutableList<String>?) = ResponseDTO(
                success = true,
                statusCode = statusCode,
                traceId = MDC.get("X-B3-TraceId"),
                timestamp = Date(),
                objects = objects,
                messages = messages
        )

        fun error(statusCode: Int, messages: MutableList<String>?) = ResponseDTO(
                success = false,
                statusCode = statusCode,
                traceId = MDC.get("X-B3-TraceId"),
                timestamp = Date(),
                messages = messages
        )
    }
}
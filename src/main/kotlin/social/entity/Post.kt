package social.entity

import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.Type
import social.entity.dto.PostDTO
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "post",
        indexes = [Index(name = "idx_post_id", columnList = "id", unique = true)]
)
data class Post(
        @Id
        @GeneratedValue(generator = "uuid2")
        @GenericGenerator(name = "uuid2", strategy = "uuid2")
        @Type(type = "uuid-char")
        var id: UUID? = null,
        var message: String,
        @ManyToOne(cascade = [CascadeType.REFRESH], targetEntity = User::class, fetch = FetchType.EAGER)
        @JoinColumn(name = "user_id", foreignKey = ForeignKey(name = "FK_POST_USER"))
        var user: User,
        var created: LocalDateTime? = null,
        @OneToMany(mappedBy = "post", cascade = [CascadeType.REMOVE], fetch = FetchType.EAGER)
        @OrderBy("created desc")
        var commentList: List<Comment>? = null
) {
    override fun toString(): String {
        return "Post(id=$id, message='$message', user=$user, created=$created, commentList=$commentList)"
    }

    fun toDTO(): PostDTO = PostDTO(
            id = this.id!!,
            message = this.message,
            user = this.user.toDTO(),
            created = this.created
    )

    companion object {

        fun fromDTO(dto: PostDTO) = Post(
                id = dto.id,
                message = dto.message!!,
                user = User.fromDTO(dto.user!!),
                created = LocalDateTime.now())
    }
}
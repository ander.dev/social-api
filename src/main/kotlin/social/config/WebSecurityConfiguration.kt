package social.config

import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.crypto.factory.PasswordEncoderFactories
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter

@Configuration
@EnableWebSecurity
@EnableConfigurationProperties
class WebSecurityConfiguration : WebSecurityConfigurerAdapter() {

    private val logger = KotlinLogging.logger {}

    @Value("\${spring.security.user.name}")
    lateinit var userName: String

    @Value("\${spring.security.user.password}")
    lateinit var userPassword: String

    @Autowired
    fun configureGlobal(auth: AuthenticationManagerBuilder) {
        try {
            auth.inMemoryAuthentication()
                    .withUser(userName)
                    .password(PasswordEncoderFactories.createDelegatingPasswordEncoder().encode(userPassword))
                    .roles("USER")
        } catch (e: Exception) {
            logger.error { e }
        }
    }

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        //This tells Spring Security to authorize all requests
        http.cors().and()
                .csrf().disable()
                .authorizeRequests()
                .mvcMatchers("/api/**")
                .authenticated()
                .and()
                .httpBasic()
    }

    @Bean
    fun corsConfigurer(): WebMvcConfigurer? {
        return object : WebMvcConfigurerAdapter() {
            override fun addCorsMappings(registry: CorsRegistry) {
                registry.addMapping("/**").allowedMethods("GET", "POST", "PUT", "DELETE").allowedOrigins("*").allowedHeaders("*")
            }
        }
    }
}
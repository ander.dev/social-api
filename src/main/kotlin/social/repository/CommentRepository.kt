package social.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import social.entity.Comment
import java.util.*

@Repository("commentRepository")
interface CommentRepository : JpaRepository<Comment, UUID> {

    fun findAllByUserIdOrderByCreated(userId: UUID): MutableList<Comment>

    fun findAllByPostIdAndUserIdOrderByCreated(postId: UUID, userId: UUID): MutableList<Comment>

}
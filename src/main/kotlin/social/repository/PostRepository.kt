package social.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import social.entity.Post
import java.util.*

@Repository("postRepository")
interface PostRepository : JpaRepository<Post, UUID> {

    fun findAllByUserIdOrderByCreatedDesc(userId: UUID): MutableList<Post>

    fun findAllByOrderByCreatedDesc(): MutableList<Post>

}
package social.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import social.entity.User
import java.util.*

@Repository("userRepository")
interface UserRepository : JpaRepository<User, UUID> {

    fun findByEmail(email: String) : Optional<User>

}
package social.exception

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import social.entity.dto.ResponseDTO
import java.util.*

@ControllerAdvice
class CustomGlobalExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(CustomException::class)
    fun customHandlingException(ex: CustomException): ResponseEntity<Any?> {
        val errors: MutableList<String>? = ArrayList()
        errors!!.add(ex.message!!)

        return ResponseEntity(ResponseDTO.error(HttpStatus.BAD_REQUEST.value(), errors), HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler(DataIntegrityViolationException::class)
    protected fun handleConstraintViolation(ex: DataIntegrityViolationException): ResponseEntity<Any> {
        val errors: MutableList<String> = ArrayList()
        val localizedMessage = ex.localizedMessage
        if (localizedMessage.contains("idx_user_email")) errors.add("Email already in use!")
        return ResponseEntity(ResponseDTO.error(HttpStatus.BAD_REQUEST.value(), errors), HttpStatus.BAD_REQUEST)
    }
}
package social.service

import social.entity.dto.ResponseDTO
import social.entity.dto.UserDTO
import java.util.*

interface UserService {

    fun save(userDTO: UserDTO) : ResponseDTO

    fun findById(id: UUID) : ResponseDTO

    fun delete(id: UUID): ResponseDTO

    fun deleteUserAutomation(email: String)

    fun login(email: String, password: String) : ResponseDTO?
}


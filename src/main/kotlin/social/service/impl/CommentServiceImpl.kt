package social.service.impl

import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import social.entity.Comment
import social.entity.Post
import social.entity.User
import social.entity.dto.CommentDTO
import social.entity.dto.ResponseDTO
import social.exception.CustomException
import social.repository.CommentRepository
import social.repository.PostRepository
import social.repository.UserRepository
import social.service.CommentService
import java.util.*
import kotlin.collections.ArrayList

@Service("commentService")
class CommentServiceImpl : CommentService {

    private val logger = KotlinLogging.logger {}

    @Autowired
    lateinit var postRepository: PostRepository

    @Autowired
    lateinit var useRepository: UserRepository

    @Autowired
    lateinit var commentRepository: CommentRepository

    private fun validateComment(commentDTO: CommentDTO): MutableList<String>? {
        val errors: MutableList<String>? = ArrayList()
        if (commentDTO.message.isNullOrEmpty()) {
            errors!!.add("Comment message must be informed!")
        }
        if (commentDTO.user == null || commentDTO.user!!.id == null) {
            errors!!.add("User must be informed!")
        }
        if (commentDTO.post == null || commentDTO.post!!.id == null) {
            errors!!.add("Post must be informed!")
        }
        return errors
    }

    @Transactional
    override fun save(commentDTO: CommentDTO): ResponseDTO {

        val comment: Comment
        val message = if (commentDTO.id == null) "Comment successfully saved!" else "Comment successfully updated!"
        try {
            val errors = validateComment(commentDTO)
            if (!errors.isNullOrEmpty()) return ResponseDTO.error(400, errors)

            val userResult: Optional<User> = useRepository.findById(commentDTO.user!!.id!!)
            if (!userResult.isPresent) return ResponseDTO.error(400, mutableListOf("User does not exists!"))
            commentDTO.user = userResult.get().toDTO()
            val postResult: Optional<Post> = postRepository.findById(commentDTO.post!!.id!!)
            if (!postResult.isPresent) return ResponseDTO.error(400, mutableListOf("Post does not exists!"))
            commentDTO.post = postResult.get().toDTO()
            comment = commentRepository.save(Comment.fromDTO(commentDTO))
        } catch (e: Exception) {
            logger.error { e }
            throw CustomException(e.message!!)
        }
        return ResponseDTO.success(200, mutableListOf(comment.toDTO()), mutableListOf(message))
    }

    override fun findAllByUser(userId: UUID): ResponseDTO {
        val userResult: Optional<User> = useRepository.findById(userId)
        if (!userResult.isPresent) return ResponseDTO.error(400, mutableListOf("User does not exists!"))

        val comments = commentRepository.findAllByUserIdOrderByCreated(userId)
        val cleanedList = removePostFromList(comments)
        val list: MutableList<Any> = ArrayList()
        cleanedList.forEach {
            list.add(it)
        }
        return ResponseDTO.success(200, list, mutableListOf("Comments successfully listed!"))
    }

    private fun removePostFromList(comments: MutableList<Comment>):MutableList<CommentDTO>{
        val cleanList: MutableList<CommentDTO> = ArrayList()
        comments.forEach{
            var dto = it.toDTO()
            dto.post = null
            cleanList.add(dto)
        }
        return  cleanList
    }

    override fun findAllByPostAndUser(postId: UUID, userId: UUID): ResponseDTO {
        val userResult: Optional<User> = useRepository.findById(userId)
        if (!userResult.isPresent) return ResponseDTO.error(400, mutableListOf("User does not exists!"))

        val postResult: Optional<Post> = postRepository.findById(postId)
        if (!postResult.isPresent) return ResponseDTO.error(400, mutableListOf("Post does not exists!"))

        val comments = commentRepository.findAllByPostIdAndUserIdOrderByCreated(postId, userId)
        val cleanedList = removePostFromList(comments)
        val list: MutableList<Any> = ArrayList()
        cleanedList.forEach {
            list.add(it)
        }
        return ResponseDTO.success(200, list, mutableListOf("Comments successfully listed!"))
    }

    override fun findById(id: UUID): ResponseDTO {
        val comment: Comment
        try {
            comment = commentRepository.findById(id).get()
        } catch (e: Exception) {
            logger.error { e }
            throw CustomException("Comment Not Found Exception")
        }
        return ResponseDTO.success(200, mutableListOf(comment.toDTO()), mutableListOf("Comment successfully Loaded!"))
    }

    override fun delete(commentId: UUID, userId: UUID): ResponseDTO {
        val userResult: Optional<User> = useRepository.findById(userId)
        if (!userResult.isPresent) return ResponseDTO.error(400, mutableListOf("User does not exists!"))

        val commentResult: Optional<Comment> = commentRepository.findById(commentId)
        if (!commentResult.isPresent) return ResponseDTO.error(400, mutableListOf("Comment does not exists!"))

        return if (userResult.get().id == commentResult.get().user.id) {
            commentRepository.deleteById(commentId)

            ResponseDTO.success(200, null, mutableListOf("Comment successfully deleted!"))
        } else {
            ResponseDTO.error(400, mutableListOf("Comment cannot be deleted as User is not the author!"))
        }
    }
}